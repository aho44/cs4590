﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;
using System.Runtime.Serialization;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class AnalysisScript : MonoBehaviour {

    public GameObject panel;
    public GameObject button;
    public GameObject cButton;
    public Dictionary<GameObject, User> dic = new Dictionary<GameObject, User>();
    public List<GameObject> buttons = new List<GameObject>();
    public static Dictionary<GameObject, DataHolder> keyValues = new Dictionary<GameObject, DataHolder>();
    public GameObject lineHolder;
    public GameObject AINav;
    public Text text;
    public Text distanceText;
    public static List<Vector3> currentPath;
    public static NavMeshAgent AINavAgent;
    bool animate = false;
	// Use this for initialization
	void Start () {
        BinaryFormatter bf = new BinaryFormatter();
        // 1. Construct a SurrogateSelector object
        SurrogateSelector ss = new SurrogateSelector();

        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3),
                        new StreamingContext(StreamingContextStates.All),
                        v3ss);

        // 2. Have the formatter use our surrogate selector
        bf.SurrogateSelector = ss;
        DirectoryInfo info = new DirectoryInfo(Application.persistentDataPath + "/cs4590/saves");
        FileInfo[] fileInfo = info.GetFiles();
        for (int i = 0; i < fileInfo.Length; i++)
        {
            //Debug.Log(fileInfo[i].ToString());
            FileStream file = File.Open(fileInfo[i].ToString(), FileMode.Open);
            User user = (User)bf.Deserialize(file);
            GameObject newButton = Instantiate(button) as GameObject;
            newButton.transform.GetChild(0).GetComponent<Text>().text = user.username;
            newButton.transform.SetParent(panel.transform);
            dic.Add(newButton, user);
            buttons.Add(newButton);
            file.Close();
        }

    }
    int count = 0;
    public void Update()
    {
        Debug.Log(animate);
        if (animate)
        {
            if (count < currentPath.Count) {
                if (!AINavAgent.pathPending)
                {
                    if (AINavAgent.remainingDistance <= AINavAgent.stoppingDistance)
                    {
                        if (!AINavAgent.hasPath || AINavAgent.velocity.sqrMagnitude == 0f)
                        {
                            AINavAgent.destination = currentPath[count];
                            count++;
                        }
                    }
                }
            }
            else
            {
                animate = false;
                count = 0;
            }
        }
    }
    public void ShowUserData()
    {
        Dictionary<GameObject, User> dictionary = GameObject.Find("Analysis").GetComponent<AnalysisScript>().dic;

        User user = dictionary[EventSystem.current.currentSelectedGameObject];
        List<GameObject> b = GameObject.Find("Analysis").GetComponent<AnalysisScript>().buttons;
        for (int i = 0; i < b.Count; i++)
        {
            Destroy(b[i]);
        }
        for (int i = 0; i < user.positions.Count; i++)
        {
            GameObject newButton = Instantiate(cButton) as GameObject;
            newButton.transform.GetChild(0).GetComponent<Text>().text = user.positions[i].state.ToString();
            newButton.transform.SetParent(GameObject.Find("Panel").transform);
            keyValues.Add(newButton, new DataHolder(user.positions[i].time, user.positions[i].pos));
        }

    }

    public void ShowPath()
    {
        Destroy(GameObject.Find("AIPlayer(Clone)"));
        Destroy(GameObject.Find("LineHolder(Clone)"));
        GameObject newLine = Instantiate(lineHolder);
        LineRenderer lRend = newLine.GetComponent<LineRenderer>();
        lRend.positionCount = keyValues[EventSystem.current.currentSelectedGameObject].list.Count;
        float distance = 0;
        for (int i = 0; i < keyValues[EventSystem.current.currentSelectedGameObject].list.Count; i++)
        {
            lRend.SetPosition(i, keyValues[EventSystem.current.currentSelectedGameObject].list[i]);
        }
        for (int i = 0; i < keyValues[EventSystem.current.currentSelectedGameObject].list.Count - 1; i++)
        {
            distance += Vector3.Distance(keyValues[EventSystem.current.currentSelectedGameObject].list[i], keyValues[EventSystem.current.currentSelectedGameObject].list[i + 1]);
        }
        currentPath = keyValues[EventSystem.current.currentSelectedGameObject].list;
        GameObject AINavPlayer = Instantiate(AINav, currentPath[0], Quaternion.identity);
        AINavAgent = AINavPlayer.GetComponent<NavMeshAgent>();
        GameObject.Find("DistanceText").GetComponent<Text>().text = "Distance traveled: " + string.Format("{0:N2}", distance);
        GameObject.Find("SecondsText").GetComponent<Text>().text = "Seconds: " + string.Format("{0:N2}", 
            keyValues[EventSystem.current.currentSelectedGameObject].time);
    }

    public void AnimatePath()
    {
        animate = true;     
    }

}
public class DataHolder
{
    public float time;
    public List<Vector3> list;
    public DataHolder(float time, List<Vector3> list)
    {
        this.time = time;
        this.list = list;
    }
}