﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Setup : MonoBehaviour {
    public CanvasGroup canvasGroup;
    public CanvasGroup cGroup;
    public Text field;
    public Text lastNameField;
    public string nextLevel;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void NextButtonClick()
    {
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = 0;
        PlayerData.instance.playerName = field.text + "_" + lastNameField.text;

        cGroup.interactable = true;
        cGroup.blocksRaycasts = true;
        cGroup.alpha = 1;
    }

    public void ExperimentButton()
    {
        SceneManager.LoadScene(nextLevel);
    }
    public void ControlButton()
    {
        PlayerData.instance.control = true;
        SceneManager.LoadScene(nextLevel);
    }
}
