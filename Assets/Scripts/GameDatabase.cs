﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameDatabase : MonoBehaviour {
    [SerializeField]
    public List<Position> actionList;
    [SerializeField]
    List<Vector3> moveList = null;
    [SerializeField]
    public GameObject lineHolder;
    [SerializeField]
    public GameObject player;
    [SerializeField]
    float timer = 0f;
    [SerializeField]
    public enum actionState
    {
        NOCOMMAND,
        SEARCHFORCEREAL
    }
    [SerializeField]
    public static actionState state;

    public static GameDatabase instance;

	// Use this for initialization
	void Start () {
        actionList = new List<Position>();
        state = actionState.NOCOMMAND;
	}
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
        DontDestroyOnLoad(this);
    }
    // Update is called once per frame
    void Update () {
		if (state == actionState.NOCOMMAND)
        {
            Entry();
            timer += Time.deltaTime;
            moveList.Add(player.transform.position);
        }
        else if (state == actionState.SEARCHFORCEREAL)
        {
            Entry();
            timer += Time.deltaTime;
            moveList.Add(player.transform.position);
        }
	}

    public void Exit()
    {
        actionList.Add(new Position(state, timer, moveList));
        moveList = null;
        timer = 0f;
    }
    public void Entry()
    {
        if (moveList == null)
        {
            moveList = new List<Vector3>();
        }

    }
    public void printList()
    {
        GameObject newLine = Instantiate(lineHolder);
        LineRenderer lRend = newLine.GetComponent<LineRenderer>();
    }
}
