﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Position {
    [SerializeField]
    public List<Vector3> pos;
    [SerializeField]
    public GameDatabase.actionState state;
    [SerializeField]
    public float time;
	public Position(GameDatabase.actionState state,float time, List<Vector3> pos)
    {
        this.state = state;
        this.time = time;
        this.pos = pos;
    }
}
