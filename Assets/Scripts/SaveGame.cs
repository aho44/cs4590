﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

public class SaveGame : MonoBehaviour {
    public GameDatabase database;
    public bool isSaveFile()
    {
        return Directory.Exists(Application.persistentDataPath + "_saves");
    }

    //public void SaveTheDamnGame()
    //{
    /*Debug.Log("I clicked this");
    saveObject.users.Add(new User("Jimmy", false, database.actionList));
    Debug.Log(saveObject.users.Count);
    User[] s = new User[saveObject.users.Count];
    for (int i = 0; i < saveObject.users.Count; i++)
    {
        s[i] = new User();
        s[i].username = saveObject.users[i].username;
        s[i].control = saveObject.users[i].control;
        s[i].positions = saveObject.users[i].positions;
    }
    User[] u = saveObject.users.ToArray();
    Debug.Log(s);
    string usersToJson = JsonHelper.ToJson(s, true);
    Debug.Log(usersToJson);
    //string jsonString = "{\r\n    \"Items\": [\r\n        {\r\n            \"playerId\": \"8484239823\",\r\n            \"playerLoc\": \"Powai\",\r\n            \"playerNick\": \"Random Nick\"\r\n        },\r\n        {\r\n            \"playerId\": \"512343283\",\r\n            \"playerLoc\": \"User2\",\r\n            \"playerNick\": \"Rand Nick 2\"\r\n        }\r\n    ]\r\n}";
    //string jsonString = "{\r\n    \"Items\": [\r\n        {\r\n            \"username\": \"8484239823\",\r\n            \"control\": \"Powai\",\r\n            \"positions\": \"Random Nick\"\r\n        },\r\n        {\r\n            \"playerId\": \"512343283\",\r\n            \"playerLoc\": \"User2\",\r\n            \"playerNick\": \"Rand Nick 2\"\r\n        }\r\n    ]\r\n}";

}*/

    public void Save()
    {
        database.Exit();
        BinaryFormatter bf = new BinaryFormatter();

        // 1. Construct a SurrogateSelector object
        SurrogateSelector ss = new SurrogateSelector();

        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3),
                        new StreamingContext(StreamingContextStates.All),
                        v3ss);

        // 2. Have the formatter use our surrogate selector
        bf.SurrogateSelector = ss;
        if (!Directory.Exists(Application.persistentDataPath + "/cs4590/saves"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/cs4590/saves");
        }
        FileStream file = File.Create(Application.persistentDataPath + "/cs4590/saves/" + PlayerData.instance.playerName + "_save");
        User user = new User();
        user.username = PlayerData.instance.playerName;
        user.control = PlayerData.instance.control;
        user.positions = database.actionList;
        bf.Serialize(file, user);
        file.Close();
    }
}
/*[Serializable]
public class SaveGameObject
{
    public List<User> users;
    public string nName;
}*/
[Serializable]
public class User
{
    [SerializeField]
    public string username;
    [SerializeField]
    public List<Position> positions;
    [SerializeField]
    public bool control;
    /*
    public User()
    {

    }

    public User(string username, bool control, List<Position> positions)
    {
        this.username = username;
        this.positions = positions;
        this.control = control;
    }*/
    
}