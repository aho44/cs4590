﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class TestScript : MonoBehaviour {
    public string[] keywords;
    public ConfidenceLevel confidence = ConfidenceLevel.Medium;
    KeywordRecognizer recognizer;
    public GameObject[] items;
    AudioSource source;
    public float speed = 1;
    public static GameObject currentItem;
    public string word = "";
    // Use this for initialization
    void Start () {
        /*for (int i = 0; i < items.Length; i++)
        {
            items[i].GetComponent<AudioSource>().Stop();
        }*/
        if (keywords != null)
        {
            recognizer = new KeywordRecognizer(keywords, confidence);
            recognizer.OnPhraseRecognized += Recognizer_OnPhraseRecognized;
            recognizer.Start();
            Debug.Log("Recognizer started");
        }
    }
    private void OnApplicationQuit()
    {
        GetComponent<GameDatabase>().Exit();
        //GetComponent<GameDatabase>().printList();
        if (recognizer != null && recognizer.IsRunning)
        {
            recognizer.OnPhraseRecognized -= Recognizer_OnPhraseRecognized;
            recognizer.Stop();
        }
    }
    private void Recognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        word = args.text;
        bool found = false;
        for (int i = 0; i < keywords.Length; i++)
        {
            if (word == keywords[i])
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            GameObject.Find("GameManager").GetComponent<GameDatabase>().Exit();
            //GameObject.Find("GameManager").GetComponent<GameDatabase>().printList();
        }
    }
    // Update is called once per frame
    void Update () {
        Debug.Log(word);
        switch (word)
        {
            case "search for cereal":
                GameDatabase.state = GameDatabase.actionState.SEARCHFORCEREAL;
                source = items[0].GetComponent<AudioSource>();
                if (!source.isPlaying)
                {
                    source.Play();
                }
                Debug.Log(items[0].GetComponent<AudioSource>());
                currentItem = items[0];
                break;
            case "cancel":
                currentItem = null;
                if (source.isPlaying)
                {
                    source.Stop();
                }
                GameDatabase.state = GameDatabase.actionState.NOCOMMAND;
                break;
        }
	}
}
